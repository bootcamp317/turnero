from django.shortcuts import render
from .models import Rol
from .forms import RolFormulario, RolModelForm, ContactForm
# from django.conf import settings

# Create your views here.

def Inicio(request):
    # print(settings.BASE_DIR)
    form = RolModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        if not instance.activo:
             instance.activo = True
        instance.save()
        print(instance)
        # form_data = form.cleaned_data
        # print(form_data)
        # aux1 = form_data.get("codigo")
        # aux2 = form_data.get("descri_rol")
        # aux3 = form_data.get("activo")
        # # obj = Rol.objects.create(codigo = aux1, descri_rol = aux2, activo = aux3)
        # try:
        #     obj = Rol()
        #     obj.codigo = aux1
        #     obj.descri_rol = aux2
        #     obj.activo = aux3
        #     obj.save()
        # except Exception as e:
        #     print("error", e)
    titulo = "Formulario Mateo"
    saludo = "Bienvenido"
    if request.user.is_authenticated:
        titulo = "Formulario %s" %(request.user)
    context = {
         "titulo":titulo,
         "titulo2":saludo,
         "el_form":form
    }
    return render(request,"Inicio.html",context)

def contact(request):
	form = ContactForm(request.POST or None)
	context = {
		"form":form,
		"titulo":"Formulario de Contacto"
}
	return render(request,"form.html",context)