from django import forms

from .models import Rol

class RolModelForm(forms.ModelForm):
    class Meta:
        model = Rol
        fields = ["codigo","descri_rol","activo"]
    def clean_codigo(self):
        codigo = self.cleaned_data.get("codigo")
        if not codigo == "valido":
            raise forms.ValidationError("Ingrese el código (valido)") 
        return codigo
        # field = '__all__'
    # Ejemplo con email
    # def clean_email(self):
	#     email = self.cleaned_data.get("email")
	#     email_base, proveedor = email.split("@) #andres@mail.edu.py
	#     aux = proveedor.split(".")
	#     dominio = aux[0]
	#     extension = aux[1]
	# if not extension == "edu":
	# 	raise forms.ValidationError("Por favor ingrese un email con extensión .EDU")
	# return email

#Independiente del model
class RolFormulario(forms.Form):
    codigo = forms.CharField(max_length=20, required=True)
    descripcion_rol = forms.CharField(max_length=20, required=False)
    activo = forms.BooleanField(required=False)

class ContactForm(forms.Form):
	nombre = forms.CharField()
	email = forms.EmailField()
	mensaje = forms.CharField(widget=forms.Textarea)