# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'

class AppEstado(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=20)

    class Meta:
        db_table = 'app_estado'


class Cliente(models.Model):
    id = models.SmallAutoField(primary_key=True)
    id_persona = models.ForeignKey('Persona', db_column='id_persona', on_delete=models.CASCADE)
    ruc = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        db_table = 'cliente'


class Departamento(models.Model):
    id = models.IntegerField(primary_key=True)
    nombre_departamento = models.CharField(max_length=20)
    lugar_atencion = models.CharField(max_length=15)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()

    class Meta:
        db_table = 'departamento'


class DepartamentoAt(models.Model):
    id_departamento = models.OneToOneField(Departamento, db_column='id_departamento', primary_key=True, on_delete=models.CASCADE)
    id_tema_atencion = models.ForeignKey('TemasAtencion', db_column='id_tema_atencion', on_delete=models.CASCADE)

    class Meta:
        db_table = 'departamento_at'
        unique_together = (('id_departamento', 'id_tema_atencion'),)


class Empleado(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    id_persona = models.ForeignKey('Persona', db_column='id_persona', on_delete=models.CASCADE)
    id_departamento = models.ForeignKey(Departamento, db_column='id_departamento', on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()

    class Meta:
        db_table = 'empleado'


class Estado(models.Model):
    id = models.SmallAutoField(primary_key=True)
    descri_estado = models.CharField(max_length=20, default="something")
    fecha_insercion = models.DateTimeField(null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()

    class Meta:
        db_table = 'estado'


class Persona(models.Model):
    id = models.SmallAutoField(primary_key=True)
    ci_num = models.CharField(max_length=8, null=True)
    nombre = models.CharField(max_length=30, null=True)
    apellido = models.CharField(max_length=30, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    direccion = models.CharField(max_length=32, blank=True, null=True)
    correo_electronico = models.CharField(max_length=15, blank=True, null=True)
    celular_num = models.CharField(max_length=12, blank=True, null=True)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()
    
    class Meta:
        db_table = 'persona'


class Prioridad(models.Model):
    id = models.SmallAutoField(primary_key=True)
    descri_prioridad = models.CharField(max_length=20)
    nivel = models.CharField(max_length=30)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()

    class Meta:
        db_table = 'prioridad'


class PuestoAtencion(models.Model):
    id = models.SmallAutoField(primary_key=True)
    nombre_puesto = models.CharField(max_length=30)

    class Meta:
        db_table = 'puesto_atencion'


class Rol(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descri_rol = models.CharField(max_length=20)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)

    def __str__(self) -> str:
        return self.codigo
    
    class Meta:
        db_table = 'rol'


class TemasAtencion(models.Model):
    id = models.SmallAutoField(primary_key=True)
    descri_tema_atencion = models.CharField(max_length=20)
    iniciales = models.CharField(max_length=3)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()

    class Meta:
        db_table = 'temas_atencion'


class Turno(models.Model):
    id = models.IntegerField(primary_key=True)
    fecha_ct = models.DateField()
    hora_ct = models.TimeField()
    fecha_atencion = models.DateField(blank=True, null=True)
    hora_atencion = models.TimeField(blank=True, null=True)
    hora_finalizacion = models.TimeField(blank=True, null=True)
    fecha_finalizacion = models.DateField(blank=True, null=True)
    duracion_espera = models.TimeField(blank=True, null=True)
    duracion_atencion = models.TimeField(blank=True, null=True)
    id_cliente = models.ForeignKey(Cliente, db_column='id_cliente', on_delete=models.CASCADE)
    id_tema_atencion = models.ForeignKey(TemasAtencion, db_column='id_tema_atencion', on_delete=models.CASCADE)
    id_prioridad = models.ForeignKey(Prioridad, db_column='id_prioridad', on_delete=models.CASCADE)
    id_estado = models.ForeignKey(Estado, db_column='id_estado', on_delete=models.CASCADE)
    id_usuario = models.ForeignKey('Usuario', db_column='id_usuario', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'turno'


class Usuario(models.Model):
    id = models.SmallAutoField(primary_key=True)
    id_empleado = models.ForeignKey(Empleado, db_column='id_empleado', on_delete=models.CASCADE)
    nombre_usuario = models.CharField(max_length=20)
    clave = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField()
    id_puesto_atencion = models.ForeignKey(PuestoAtencion, db_column='id_puesto_atencion', on_delete=models.CASCADE)

    class Meta:
        db_table = 'usuario'


class UsuarioRol(models.Model):
    id_usuario = models.OneToOneField(Usuario, db_column='id_usuario', primary_key=True, on_delete=models.CASCADE)
    id_rol = models.ForeignKey(Rol, db_column='id_rol', on_delete=models.CASCADE)

    class Meta:
        db_table = 'usuario_rol'
        unique_together = (('id_usuario', 'id_rol'),)
