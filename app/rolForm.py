from django import forms

class RolFormulario(forms.Form):
    codigo = forms.CharField(max_length=20, required=True)
    descripcion_rol = forms.CharField(max_length=20, required=False)
    activo = forms.BooleanField(required=False)
    """
    Ejemplo
    descripcion = forms.CharField(max_length=20, required=True)
    estado = forms.BooleanField(required=False)
    """