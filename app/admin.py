from django.contrib import admin
from . import models
from .forms import RolModelForm
# Register your models here.

class RolAdmin(admin.ModelAdmin):
    list_display=["__str__","descri_rol", "activo", "fecha_insercion"]
    list_editable=["descri_rol"]
    list_filter=["fecha_insercion"]
    search_fields=["codigo"]
    form = RolModelForm

admin.site.register(models.Rol, RolAdmin)