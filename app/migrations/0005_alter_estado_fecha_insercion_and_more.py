# Generated by Django 4.1.7 on 2023-02-24 01:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_appestado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estado',
            name='fecha_insercion',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='prioridad',
            name='fecha_insercion',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='rol',
            name='fecha_insercion',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
