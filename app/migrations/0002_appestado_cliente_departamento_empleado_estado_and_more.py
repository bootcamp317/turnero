# Generated by Django 4.1.7 on 2023-02-23 21:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AppEstado',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'app_estado',
            },
        ),
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('ruc', models.CharField(blank=True, max_length=10, null=True)),
            ],
            options={
                'db_table': 'cliente',
            },
        ),
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('nombre_departamento', models.CharField(max_length=20)),
                ('lugar_atencion', models.CharField(max_length=15)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
            ],
            options={
                'db_table': 'departamento',
            },
        ),
        migrations.CreateModel(
            name='Empleado',
            fields=[
                ('id', models.SmallIntegerField(primary_key=True, serialize=False)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
                ('id_departamento', models.ForeignKey(db_column='id_departamento', on_delete=django.db.models.deletion.CASCADE, to='app.departamento')),
            ],
            options={
                'db_table': 'empleado',
            },
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descri_estado', models.CharField(default='something', max_length=20)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
            ],
            options={
                'db_table': 'estado',
            },
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('ci_num', models.CharField(max_length=8, null=True)),
                ('nombre', models.CharField(max_length=30, null=True)),
                ('apellido', models.CharField(max_length=30, null=True)),
                ('fecha_nacimiento', models.DateField(blank=True, null=True)),
                ('direccion', models.CharField(blank=True, max_length=32, null=True)),
                ('correo_electronico', models.CharField(blank=True, max_length=15, null=True)),
                ('celular_num', models.CharField(blank=True, max_length=12, null=True)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
            ],
            options={
                'db_table': 'persona',
            },
        ),
        migrations.CreateModel(
            name='Prioridad',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descri_prioridad', models.CharField(max_length=20)),
                ('nivel', models.CharField(max_length=30)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
            ],
            options={
                'db_table': 'prioridad',
            },
        ),
        migrations.CreateModel(
            name='PuestoAtencion',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('nombre_puesto', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'puesto_atencion',
            },
        ),
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('codigo', models.CharField(max_length=8)),
                ('descri_rol', models.CharField(max_length=20)),
                ('activo', models.BooleanField()),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'rol',
            },
        ),
        migrations.CreateModel(
            name='TemasAtencion',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('descri_tema_atencion', models.CharField(max_length=20)),
                ('iniciales', models.CharField(max_length=3)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
            ],
            options={
                'db_table': 'temas_atencion',
            },
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.SmallAutoField(primary_key=True, serialize=False)),
                ('nombre_usuario', models.CharField(max_length=20)),
                ('clave', models.CharField(max_length=20)),
                ('fecha_insercion', models.DateTimeField()),
                ('fecha_modificacion', models.DateTimeField(blank=True, null=True)),
                ('activo', models.BooleanField()),
                ('id_empleado', models.ForeignKey(db_column='id_empleado', on_delete=django.db.models.deletion.CASCADE, to='app.empleado')),
                ('id_puesto_atencion', models.ForeignKey(db_column='id_puesto_atencion', on_delete=django.db.models.deletion.CASCADE, to='app.puestoatencion')),
            ],
            options={
                'db_table': 'usuario',
            },
        ),
        migrations.CreateModel(
            name='Turno',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('fecha_ct', models.DateField()),
                ('hora_ct', models.TimeField()),
                ('fecha_atencion', models.DateField(blank=True, null=True)),
                ('hora_atencion', models.TimeField(blank=True, null=True)),
                ('hora_finalizacion', models.TimeField(blank=True, null=True)),
                ('fecha_finalizacion', models.DateField(blank=True, null=True)),
                ('duracion_espera', models.TimeField(blank=True, null=True)),
                ('duracion_atencion', models.TimeField(blank=True, null=True)),
                ('id_cliente', models.ForeignKey(db_column='id_cliente', on_delete=django.db.models.deletion.CASCADE, to='app.cliente')),
                ('id_estado', models.ForeignKey(db_column='id_estado', on_delete=django.db.models.deletion.CASCADE, to='app.estado')),
                ('id_prioridad', models.ForeignKey(db_column='id_prioridad', on_delete=django.db.models.deletion.CASCADE, to='app.prioridad')),
                ('id_tema_atencion', models.ForeignKey(db_column='id_tema_atencion', on_delete=django.db.models.deletion.CASCADE, to='app.temasatencion')),
                ('id_usuario', models.ForeignKey(blank=True, db_column='id_usuario', null=True, on_delete=django.db.models.deletion.CASCADE, to='app.usuario')),
            ],
            options={
                'db_table': 'turno',
            },
        ),
        migrations.AddField(
            model_name='empleado',
            name='id_persona',
            field=models.ForeignKey(db_column='id_persona', on_delete=django.db.models.deletion.CASCADE, to='app.persona'),
        ),
        migrations.AddField(
            model_name='cliente',
            name='id_persona',
            field=models.ForeignKey(db_column='id_persona', on_delete=django.db.models.deletion.CASCADE, to='app.persona'),
        ),
        migrations.CreateModel(
            name='UsuarioRol',
            fields=[
                ('id_usuario', models.OneToOneField(db_column='id_usuario', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='app.usuario')),
                ('id_rol', models.ForeignKey(db_column='id_rol', on_delete=django.db.models.deletion.CASCADE, to='app.rol')),
            ],
            options={
                'db_table': 'usuario_rol',
                'unique_together': {('id_usuario', 'id_rol')},
            },
        ),
        migrations.CreateModel(
            name='DepartamentoAt',
            fields=[
                ('id_departamento', models.OneToOneField(db_column='id_departamento', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='app.departamento')),
                ('id_tema_atencion', models.ForeignKey(db_column='id_tema_atencion', on_delete=django.db.models.deletion.CASCADE, to='app.temasatencion')),
            ],
            options={
                'db_table': 'departamento_at',
                'unique_together': {('id_departamento', 'id_tema_atencion')},
            },
        ),
    ]
